<?php 
session_start();
error_reporting(0);

  $num_cuenta =$_POST['num_cuenta'];
     $nombre=$_POST['nombre'];
     $primerApellido=$_POST['primer_apellido'];
     $segundoApellido=$_POST['segundo_apellido'];
     $genero=$_POST['genero'];
     $fechaNac=$_POST['fecha_nac'];
     $contrasenia=$_POST['contrasenia'];


       $_SESSION['Alumno'] = [            
          1=>[
               'num_cuenta' => '1',
               'nombre' => 'Admin',
               'primer_apellido' => 'General',
               'segundo_apellido' => '',
               'contrasenia' => 'adminpass123',
               'genero' => 'O',
               'fecha_nac'=> '28/02/1994'
          ],
          2=>[               
               'num_cuenta' =>$num_cuenta,
               'nombre' => $nombre,
               'primer_apellido' => $primerApellido,
               'segundo_apellido' => $segundoApellido,
               'contrasenia' => $contrasenia,
               'genero' => $genero,
               'fecha_nac'=> $fechaNac
      
          ]
     ];
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="estilos.css">
          <title>Home</title>

</head>
     
<body>


<header>
          <div class="menu">
               <nav>
                    <ul>
                         <li><a href="#">Home</a></li>
                         <li><a href="formulario.php">Registar Alumnos</a></li>
                         <li><a href="cerrar.php">Cerrar Sesión</a></li>
                    </ul>
               </nav>
          </div>
          
     </header> 
    <main>
    <section id="section">
     
     <div class="div">
          <h2>Usuario Autenticado</h2>
          <table class="tabla-autUsuarios">
               <thead class = thead-autUsuario>
                    <tr><th>Admin General</th></tr>
               </thead>
               <?php foreach($_SESSION['Alumno'] as $alumno => $value):?>
               <tr><td class="informacion"><h2> Información</h2>
               <br>
               <?php echo"Numero de cuenta: " .$value['num_cuenta']?><br><br>

               <?php echo"Fecha de Nacimiento: ".$value['fecha_nac']?>
               </td></tr>
               <?php endforeach ?>
          </table>

     </div>
     </section>

     <section id="section">
          <div class="div">
               <h1>Datos guardados</h1>
               <table class="tabla-datGuardados">
               
                    <thead>                    
                         <tr class="sin-borde">
                              <th>#</th>
                              <th>Nombre</th>
                              <th>Fecha de Nacimiento</th>
                         </tr>
                    </thead>
                    <?php foreach($_SESSION['Alumno'] as $alumno => $value):?>
                    <tr>
                         <td class="datos columna"><?php echo $value['num_cuenta'] ?></td>
                         <td class="datos"><?php echo $value['nombre'] ?></td>
                         <td class="datos"><?php echo $value['fecha_nac'] ?></td>
                    </tr>
                    <?php endforeach ?>
               </table>
          </div>
     </section>
    </main>
     
</body>
</html>