<?php
if (isset($_POST['submit'])){
	$curp = $_POST['curp'];
	$expresion= '/^([A-Z&]|[a-z&]{1})([AEIOU]|[aeiou]{1})([A-Z&]|[a-z&]{1})([A-Z&]|[a-z&]{1})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([HM]|[hm]{1})([AS|as|BC|bc|BS|bs|CC|cc|CS|cs|CH|ch|CL|cl|CM|cm|DF|df|DG|dg|GT|gt|GR|gr|HG|hg|JC|jc|MC|mc|MN|mn|MS|ms|NT|nt|NL|nl|OC|oc|PL|pl|QT|qt|QR|qr|SP|sp|SL|sl|SR|sr|TC|tc|TS|ts|TL|tl|VZ|vz|YN|yn|ZS|zs|NE|ne]{2})([^A|a|E|e|I|i|O|o|U|u]{1})([^A|a|E|e|I|i|O|o|U|u]{1})([^A|a|E|e|I|i|O|o|U|u]{1})([0-9]{2})$/m';


	if(preg_match($expresion, $curp)){
		echo "<script>alert('El CURP ingresado es correcto');</script>";
		
	}else {
		echo "<script>alert('Ingresa los datos nuevamente');</script>";
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Validación de CURP</title>
</head>
<body>
	<center><h1>Verificación de CURP</h1>
		<form action="" method="post">
			Ingresa la CURP a validar: <input type="text" name="curp">
			<p><input type="submit" name="submit" value="Enviar"></p>
		</form>
 	</center>
</body>
</html>